﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Shortcode.Parser;

namespace Shortcode.Test
{
    /// <summary>
    /// This test is meant to test different types of situations for shortcodes and parsing
    /// </summary>
    [TestFixture]
    public class TestShortcodeParser
    {
        ShortcodeParser parser;

        [SetUp]
        public void setup()
        {
            parser = new ShortcodeParser(
                new StaticHandler("static", "my_clean"),
                new StaticHandler("foo-hypen-yah", "my_bar"),
                new StaticHandler("test-shortcode-tag", "return test-shortcode-tag"),
                new DelegateHandler("footag", delegate(Dictionary<string, string> atts, string inner)
                {
                    return "foo = " + (atts.ContainsKey("foo") ? atts["foo"] : string.Empty);
                }),
                new DelegateHandler("bar-tag", delegate(Dictionary<string, string> atts, string inner)
                {
                    return "{" + string.Join(",", atts.Select(d => string.Format("\"{0}\":\"{1}\"", d.Key, d.Value))) + "}";
                }),
                new DelegateHandler("content", delegate(Dictionary<string, string> atts, string inner)
                {
                    return inner;
                })
            );
        }

        [Test]
        public void TestTags()
        {
            // Test escaping
            Assert.AreEqual(@"[static]", parser.Parse(@"[[static]]"));
            Assert.AreEqual(@"[static /]", parser.Parse(@"[[static /]]"));
            // TODO: fix this edge case?
            //Assert.AreEqual(@"[my_clean", parser.Parse(@"[[static]"));

            // Test normal
            Assert.AreEqual(@"my_clean", parser.Parse(@"[static]"));
            Assert.AreEqual(@"  my_clean", parser.Parse(@"  [static     ]"));
            Assert.AreEqual(@"my_clean  ", parser.Parse(@"[static /]  "));
            Assert.AreEqual(@"  my_clean ", parser.Parse(@"  [static ][/static] "));
            Assert.AreEqual(@"my_clean", parser.Parse(@"[static ]asdfasdf[/static]"));
            Assert.AreEqual(@"  my_clean", parser.Parse(@"  [static foo=""bar""]asdfasdf[/static]"));
            Assert.AreEqual(@"my_cleanasdfasdf[/foo]", parser.Parse(@"[static foo=""bar""]asdfasdf[/foo]"));

            // Test hyphens
            Assert.AreEqual(@"my_bar", parser.Parse(@"[foo-hypen-yah]"));
            Assert.AreEqual(@"  my_bar", parser.Parse(@"  [foo-hypen-yah     ]"));
            Assert.AreEqual(@"my_bar  ", parser.Parse(@"[foo-hypen-yah /]  "));
            Assert.AreEqual(@"  my_bar  ", parser.Parse(@"  [foo-hypen-yah ][/foo-hypen-yah]  "));
            Assert.AreEqual(@"my_bar", parser.Parse(@"[foo-hypen-yah ]asdfasdf[/foo-hypen-yah]"));
            Assert.AreEqual(@"fdsamy_barasdf", parser.Parse(@"fdsa[foo-hypen-yah ]asdfasdf[/foo-hypen-yah]asdf"));
            Assert.AreEqual(@"  my_bar", parser.Parse(@"  [foo-hypen-yah foo=""bar""]asdfasdf[/foo-hypen-yah]"));
            Assert.AreEqual(@"my_barasdfasdf[/foo]", parser.Parse(@"[foo-hypen-yah foo=""bar""]asdfasdf[/foo]"));
        }

        [Test]
        public void TestAttributes()
        {
            Assert.AreEqual(@"{}", parser.Parse(@"[bar-tag /]"));
            Assert.AreEqual(@"{""foo"":""bar""}", parser.Parse(@"[bar-tag      foo=""bar"" /]"));
            Assert.AreEqual(@"a{""foo"":""bar"",""baz"":""bang""}b", parser.Parse(@"a[bar-tag foo=""bar"" baz='bang'/]b"));
            Assert.AreEqual(@"[not-a-tag foo=""bar""]", parser.Parse(@"[not-a-tag foo=""bar""]"));
        }

        [Test]
        public void TestInnerContent()
        {
            Assert.IsEmpty(parser.Parse(@"[content /]"));
            Assert.IsEmpty(parser.Parse(@"[content     /]"));
            Assert.IsEmpty(parser.Parse(@"[content     ]"));
            Assert.IsEmpty(parser.Parse(@"[content  ][/content]"));
            Assert.AreEqual(" ", parser.Parse(@"[content  ] [/content]"));
            Assert.AreEqual("asdf", parser.Parse(@"[content  ]asdf[/content]"));
        }

        [Test]
        public void TestAll()
        {
            Assert.AreEqual(@"content [static] my_cleanasd[not-a-tag] f foo = baznull{""a"":""b"",""c"":""d""}", parser.Parse(@"content [[static]] [static]asd[not-a-tag] f [footag foo=""baz""][content]null[/content][bar-tag a=""b"" c=d]"));
        }
    }


    // Helper classes are below


    class DelegateHandler : ShortcodeTagHandler
    {
        public delegate string CheckHandle(Dictionary<string, string> attributesAndValues, string innerContent);
        private CheckHandle _checkHandleFunc;

        public DelegateHandler(string tag, CheckHandle checkHandleFunc)
            : base(tag)
        {
            _checkHandleFunc = checkHandleFunc;
        }

        public override string Handle(Dictionary<string, string> attributesAndValues, string innerContent)
        {
            return _checkHandleFunc(attributesAndValues, innerContent);
        }
    }

    class StaticHandler : DelegateHandler
    {
        public StaticHandler(string tag, string retVal)
            : base(tag, delegate(Dictionary<string, string> attributesAndValues, string innerContent) { return retVal; })
        { }
    }
}
