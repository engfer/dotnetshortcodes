﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shortcode.Parser;

namespace Shortcode
{
    class OpentextVideoHandler : ShortcodeTagHandler
    {
        public OpentextVideoHandler() : base("video") { }

        public override string Handle(Dictionary<string, string> attributesAndValues, string innerContent)
        {
            return "<span>da_video</span>";
        }

    }

    class Program
    {
        /// <summary>
        ///  Driver.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            ShortcodeParser parser = new ShortcodeParser(new[] {
                new OpentextVideoHandler()
            });

            String html = "html [video k=\"v\" k2=v2/]a [[video]] [lala/] text";
            Console.WriteLine("---------");
            Console.WriteLine(parser.Parse(html));
            Console.ReadLine();
        }
    }
}
