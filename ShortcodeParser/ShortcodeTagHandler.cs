﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shortcode.Parser
{
    /// <summary>
    /// A shortcode tag handler defines a handler for anytime "TagName" appears the content parsed by the ShortcodeParser. Ex: '[foobar k="v"]bobo[/foobar]' 
    /// "foobar" would be the TagName and the Handle method would have {"k" => "v"} passed as its dictionary attributes, and would have 'bobo' passed as 
    /// its innerContent. The content returned from the Handle() method will replace any instance of the tag. Tag names are case sensitive. You must 
    /// override this and provide an implementation that replaces the tag the content you want.
    /// </summary>
    public abstract class ShortcodeTagHandler
    {
        /// <summary>
        /// The shortcode tag name to 
        /// </summary>
        public string TagName { get; protected set; }

        /// <summary>
        /// Create an instance the tag handler that will be responsible for replacing any tags with the provided tagName.
        /// </summary>
        /// <param name="tagName">the case-sensitive name of tag to be handled</param>
        protected ShortcodeTagHandler(string tagName)
        {
            if (string.IsNullOrEmpty(tagName))
            {
                throw new ArgumentException("A tagName must be provided");
            }
            TagName = tagName;
        }

        /// <summary>
        /// For ever instance of the [TagName] found by the parser, this method will be called to replace the tag with the content returned from this method.
        /// If the tag was provided any attributes, those are passed in as a dectionary. If the tag is not self-closing, the innerContent is provided as well.
        /// </summary>
        /// <param name="attributesAndValues">map of attribute key=value pairs that existed on the tag</param>
        /// <param name="innerContent">if the shortcode tag is not a self-closing tag, this would be the content inside of a shotcode. null if self-closing.</param>
        /// <returns></returns>
        public abstract string Handle(Dictionary<string, string> attributesAndValues, string innerContent);
    }
}
