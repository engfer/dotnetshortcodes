﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Shortcode.Parser
{
    /// <summary>
    /// This is an implementation of the Wordpress (3.5.1) shortcode parser http://codex.wordpress.org/Shortcode_API. Most of this code/logic was lifted 
    /// from http://bit.ly/XZj6l4 and adapted for C#. The main changes were to migrate the PHP PCRE regex syntax to a C#-ified version.
    /// 
    /// In essence, you can have html code contain shortcodes (e.g. '[shortcode]') that will be replaced prior to rendering, but don't interfere with 
    /// WYSIWYG editors. This operates much like BBCode if you are familiar with that. The Parse() is the meat-and-bones that replaces the shortcodes
    /// found in the provided html with the content returned from their respective tag handlers'. For instance, a shortcode tag handler for javascript 
    /// insertion might perform the following: if 'blah blah [script src="foo.js"/] blah blah' was passed to the parser that contained a tag handler for
    /// 'script', the content returned from Parse() would look like 'blah blah <script src="foo.js"></script> blah blah'.
    /// 
    /// Supported formats [foo], [foo /], [foo]inner[/foo]. Attributes can be added after the start tag as well (like HTML).
    /// 
    /// If no handler is found (case-sensitive) for a found shortcode, the 'shortcode' is left alone. To escape a shortcode, use double square brackets
    /// like [[foo]].
    /// </summary>
    public class ShortcodeParser
    {
        /// <summary>
        /// This is the shortcode search regex pattern matcher that is combined with the RegexEnd and the tag names. There are a series of sub-matches as
        /// well. Possesive matchers '*+' were not supported by .NET Regex, so that content had to be changed to (?>*). For example '(?:pattern)*+' would
        /// have to be changed to '(?>(?:pattern)*). Remember, this is lifted and adapted from Wordpress shortcode.
        /// </summary>
        const string RegexStart = @"\[" // Opening bracket
            + @"(\[?)"                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]];
            + @"(";                               // 2: Shortcode names start

        /// <summary>
        /// See the comments for RegexStart. Remember, this is lifted and adapted from Wordpress shortcode.
        /// </summary>
        const string RegexEnd = @")" // 2: Shortcode names end
            + @"(?![\w-])"                       // Not followed by word character or hyphen
            + @"("                                // 3: Unroll the loop: Inside the opening shortcode tag
            + @"[^\]\/]*"                   // Not a closing bracket or forward slash
            + @"(?:"
            + @"\/(?!\])"               // A forward slash not followed by a closing bracket
            + @"[^\]\/]*"               // Not a closing bracket or forward slash
            + @")*?"
            + @")"
            + @"(?>(?:"
            + @"(\/)"                        // 4: Self closing tag ...
            + @"\]"                          // ... and closing bracket
            + @"|"
            + @"\]"                          // Closing bracket
            + @"(?:"
            + @"("                        // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags
            + @"(?>[^\[]*)"             // Not an opening bracket
            + @"(?:"
            + @"\[(?!\/\2\])" // An opening bracket not followed by the closing shortcode tag
            + @"(?>[^\[]*)"         // Not an opening bracket
            + @")*)"
            + @")"
            + @"\[\/\2\]"             // Closing shortcode tag
            + @")?"
            + @")"
            + @"(\]?)";                          // 6: Optional second closing brocket for escaping shortcodes: [[tag]]

        /// <summary>
        /// This is the regex string used to parse out any attributes provided on the tag. Remember, this is lifted and adapted from Wordpress shortcode. 
        /// The only changes made were to escape the double quotes.
        /// </summary>
        const string AttributeRegex = @"(\w+)\s*=\s*""([^""]*)""(?:\s|$)|(\w+)\s*=\s*\'([^\']*)\'(?:\s|$)|(\w+)\s*=\s*([^\s\'""]+)(?:\s|$)|""([^""]*)""(?:\s|$)|(\S+)(?:\s|$)";

        /// <summary>
        /// This represents the case-sensitive map of shortcode tag handlers that will handle the replacement of the tags.
        /// </summary>
        public Dictionary<string, ShortcodeTagHandler> ShortcodeTagHandlers { get; private set; }
        /// <summary>
        /// The generated regex string for finding shortcodes. Essentially its RegexStart + (tag names separated by '|') + RegexEnd
        /// </summary>
        private readonly string _shortcodeTagHandlersRegex;

        /// <summary>
        /// Create a parser for the provided tag handlers
        /// </summary>
        /// <param name="shortcodeTagHandlers">vararg list of shortcode tag handlers (must be at least one)</param>
        public ShortcodeParser(params ShortcodeTagHandler[] shortcodeTagHandlers)
        {
            if (shortcodeTagHandlers == null || !shortcodeTagHandlers.Any())
            {
                throw new ArgumentException("must provide at least one shortcode tag handler");
            }

            // Use a dictionary for constant time lookup
            ShortcodeTagHandlers = shortcodeTagHandlers.ToDictionary(key => key.TagName, val => val);
            // Pre-create the regex string
            _shortcodeTagHandlersRegex = GetShortcodeRegex(ShortcodeTagHandlers);
        }

        /// <summary>
        /// The meat-and-bones method that take the provided HTML and replaces any shortcodes with the content for the handler for that tag. If no handler
        /// is found for the shortcode, its left as-is. See the class description for more detail. A new string of the 'pruned' HTML is returned.
        /// Ex: var prunedHTML = parser.Parse(HTML);
        /// </summary>
        /// <param name="html">HTML string which could contain shortcodes</param>
        /// <returns>the original HTML string with the shortcodes replaced (by matching tag handlers)</returns>
        public string Parse(string html)
        {
            // For each shortcode match (with containing sub-matches), ReplaceShortcodeTagMatch delegate will be called
            return  !string.IsNullOrWhiteSpace(html) ? Regex.Replace(html, _shortcodeTagHandlersRegex, ReplaceShortcodeTagMatch) : html;
        }

        /// <summary>
        /// Regex.Replace delegate for an individual shortcode match that was found in the HTML.
        /// </summary>
        /// <param name="match">a match containing sub-matches for a found shortcode</param>
        /// <returns>replacement string for the matched shortcode</returns>
        private string ReplaceShortcodeTagMatch(Match match)
        {
            // allow [[foo]] syntax for escaping a tag
            if (match.Groups[1].Value == "[" && match.Groups[6].Value == "]")
            {
                return !string.IsNullOrEmpty(match.Groups[0].Value) ? match.Groups[0].Value.Substring(1, match.Groups[0].Value.Length - 2) : string.Empty;
            }

            var shortCodeTag = match.Groups[2].Value;
            var attributesAndValues = ParseShortcodeAttributes(match.Groups[3].Value);

            var shortCodeResult = DoShortcodeFor(shortCodeTag, attributesAndValues, !string.IsNullOrEmpty(match.Groups[5].Value) ? match.Groups[5].Value : null);
            return match.Groups[1].Value + shortCodeResult + (match.Groups[6].Value != "]" ? match.Groups[6].Value : string.Empty);
        }

        /// <summary>
        /// For each shortcode match, call the handler for the provided short code tag with any attributes and inner content.
        /// </summary>
        /// <param name="shortCodeTag">the handler tag to search for</param>
        /// <param name="attributes">any attributes found on the tag</param>
        /// <param name="innerContent">if not a self-closing tag, this is the content between the open and closing tag</param>
        /// <returns>data from the tag handler, or empty string if no handler found</returns>
        private string DoShortcodeFor(string shortCodeTag, Dictionary<string, string> attributes, string innerContent)
        {
            if (!string.IsNullOrEmpty(shortCodeTag) && ShortcodeTagHandlers.ContainsKey(shortCodeTag))
            {
                return ShortcodeTagHandlers[shortCodeTag].Handle(attributes, innerContent);
            }
            return string.Empty;
        }

        /// <summary>
        /// For the provided attribute string, parse out the attribute key=value pairs and turn them into a dictionary.
        /// </summary>
        /// <param name="attributeStr"></param>
        /// <returns>attributes mapped in attr => attrValue format or an empty dictionary</returns>
        private static Dictionary<string, string> ParseShortcodeAttributes(string attributeStr)
        {
            var attributesAndValues = new Dictionary<string, string>();
            var text = Regex.Replace(attributeStr, @"[\u00a0\u200b]+", " ");

            MatchCollection matches = Regex.Matches(attributeStr, AttributeRegex);

            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    if (!string.IsNullOrEmpty(match.Groups[1].Value))
                    {
                        attributesAndValues.Add(match.Groups[1].Value.ToLower(), StripCSlashes(match.Groups[2].Value));
                    }
                    else if (!string.IsNullOrEmpty(match.Groups[3].Value))
                    {
                        attributesAndValues.Add(match.Groups[3].Value.ToLower(), StripCSlashes(match.Groups[4].Value));
                    }
                    else if (!string.IsNullOrEmpty(match.Groups[5].Value))
                    {
                        attributesAndValues.Add(match.Groups[5].Value.ToLower(), StripCSlashes(match.Groups[6].Value));
                    }
                    else if (!string.IsNullOrEmpty(match.Groups[7].Value))
                    {
                        attributesAndValues.Add(match.Groups[7].Value.ToLower(), StripCSlashes(match.Groups[7].Value));
                    }
                    else if (!string.IsNullOrEmpty(match.Groups[8].Value))
                    {
                        attributesAndValues.Add(match.Groups[7].Value.ToLower(), StripCSlashes(match.Groups[8].Value));
                    }

                }
            }
            else
            {
                // TODO: what? I have no idea what use-case this solves?
                //$atts = ltrim($text);
            }

            return attributesAndValues;
        }

        /// <summary>
        /// Generate the shortcode regex string for all of the provided supported shortcode tags.
        /// </summary>
        /// <param name="shortcodeTagHandlers"></param>
        /// <returns>Regex replacement string with submatch groups/etc</returns>
        private static string GetShortcodeRegex(Dictionary<string, ShortcodeTagHandler> shortcodeTagHandlers)
        {
            var shortcodeTagsRegex = string.Join("|", shortcodeTagHandlers.Select(pair => pair.Value.TagName));

            return RegexStart + shortcodeTagsRegex + RegexEnd;
        }

        /// <summary>
        /// This is an implementation of PHP's stripcslashes(). It removes the "\" from the provided string returns a new altered string.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>string with backslashes removed</returns>
        private static string StripCSlashes(string input)
        {
            return !string.IsNullOrEmpty(input) ? input.Replace(@"\", "") : input;
        }
    }
}
